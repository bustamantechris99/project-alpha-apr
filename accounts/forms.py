from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(
        max_length=150,
        widget=forms.TextInput(attrs={"placeholder": "username"}),
    )
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(attrs={"placeholder": "password"}),
    )


class CreateForm(forms.Form):
    username = forms.CharField(
        max_length=150,
        widget=forms.TextInput(attrs={"placeholder": "username"}),
    )
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(attrs={"placeholder": "password"}),
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput(attrs={"placeholder": "password"}),
    )
