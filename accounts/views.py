from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm, CreateForm
from django.contrib.auth.models import User


def LogIn(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("home")
            else:
                return HttpResponse("Incorrect entries!")
    else:
        form = LoginForm()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def LogOut(request):
    logout(request)
    return redirect("login")


def CreateUser(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username, email=None, password=password
                )
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "Passwords do not match!")
    else:
        form = CreateForm(request.POST)
    context = {"form": form}
    return render(request, "accounts/signup.html", context)
